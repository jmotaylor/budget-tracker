import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';

ThemeData customTheme() {
  return ThemeData(
    primaryColor: Styles.primaryColor,
    cursorColor: Styles.textColorDefault,
    cardTheme: CardTheme(
      color: Styles.secondaryColor,
    ),
    cardColor: Styles.primaryColor
  );
}
