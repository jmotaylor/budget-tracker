import 'package:budget_tracker/sp_functions/sp_functions.dart';

class Balance {
  String balance;
  Map map = {};

  Balance(this.balance) {
    map["balance"] = balance;
  }

  void save() {
    saveMap(map);
  }
}