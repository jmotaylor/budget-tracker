import 'dart:convert';

import 'package:budget_tracker/helpers/enums.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';

class AccountingEntry {
  String key;
  InputType inputType;
  String amount;
  String category;
  String notes;
  DateTime dateCreated;

  AccountingEntry(
      {this.key,
      this.inputType,
      this.amount,
      this.category,
      this.notes,
      this.dateCreated});

  Future<Map<String, dynamic>> toJson() async {
    String type = inputType.toString();
    String date = dateCreated.toString();
    return {
      'key': key,
      'inputType': type,
      'amount': amount,
      'notes': notes,
      'category': category,
      'dateCreated': date
    };
  }

  Future<void> save(String key) async {
    String json = jsonEncode(await this.toJson());
    // String key = await getKey(inputType);

    saveMap({key: json});
  }

  AccountingEntry.fromJson(Map json)
      : key = json['key'],
        inputType = InputType.values.firstWhere((element) => element.toString() == json['inputType']),
        amount = json['amount'],
        notes = json['notes'],
        category = json['category'],
        dateCreated = DateTime.parse(json['dateCreated']);
}
