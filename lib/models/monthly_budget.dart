import 'package:budget_tracker/sp_functions/sp_functions.dart';

class MonthlyBudget {
  String budget;
  String balance;
  int currency;
  Map map = {};

  MonthlyBudget(this.budget, this.currency) {
    map["budget"] = budget;
    map["currency"] = currency;
  }

  void save() {
    saveMap(map);
  }
}