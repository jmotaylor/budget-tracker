import 'package:budget_tracker/sp_functions/sp_functions.dart';

class Currency {
  int id;
  String name;
  String symbol;
  bool decimals;

  Currency(this.id, this.name, this.symbol, this.decimals);

  static List<Currency> getCurrencies() {
    return <Currency>[
      Currency(0, 'Dollars', '\$', true),
      Currency(1, 'Yen', '¥', false),
      Currency(2, 'Pesos', '₱', true),
      Currency(3, 'None (decimal)', null, true),
      Currency(4, 'None (no decimal)', null, false),
    ];
  }

  static Future<Currency> getSavedCurrency() async {
    List<Currency> _currencies = Currency.getCurrencies();
    int rawCurrency = await get("currency", int);
    // Map currency = {};

    for (Currency thisCurrency in _currencies) {
      if (thisCurrency.id == rawCurrency) {
        return thisCurrency;
      }
    }
  }
}
