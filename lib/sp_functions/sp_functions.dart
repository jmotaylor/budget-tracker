import 'dart:math';
import 'package:budget_tracker/helpers/enums.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> saveMap(Map object) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  for (String key in object.keys) {
    Type type = object[key].runtimeType;

    switch (type) {
      case String:
        _prefs.setString(key, object[key]);
        break;
      case List:
        _prefs.setStringList(key, object[key]);
        break;
      case int:
        _prefs.setInt(key, object[key]);
        break;
      case double:
        _prefs.setDouble(key, object[key]);
        break;
      case bool:
        _prefs.setBool(key, object[key]);
        break;
      default:
        throw ("The value's type of $type is not supported by Shared Preferences");
    }
  }
}

Future<String> createKey(InputType inputType) async {
  // Get type
  String type = inputType.toString();
  // Get all keys that start with type
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  Set<String> keys = _prefs.getKeys();
  List<int> ids = [];

  // Loop through each key and check if they match with type
  for (String key in keys) {
    if (key.contains(type)) {
      // Remove type to get remaining id
      String reducedKey = key.replaceAll(type, '');
      ids.add(int.parse(reducedKey));
    }
  }
  if (ids.length != 0) {
    // Add 1 to id
    int id = ids.reduce(max) + 1;
    return type + id.toString();
  } else {
    return type + "1";
  }
}

Future<Set<String>> getKeys() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  Set<String> keys = _prefs.getKeys();
  return keys;
}

Future<List<String>> getAllKeysWithPrefixOf(dynamic term) async {
  if (term.runtimeType == InputType || term.runtimeType == String) {
    term = term.toString();

    List<String> keyList = [];

    Set<String> keys = await getKeys();

    for (String key in keys) {
      if (key.contains(term)) {
        keyList.add(key);
      }
    }

    return keyList;
  } else {
    throw ("Argument must be of InputType or String");
  }
}

Future<dynamic> get(String key, Type type) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  dynamic value;

  switch (type) {
    case String:
      value = _prefs.getString(key);
      break;
    case List:
      value = _prefs.getStringList(key);
      break;
    case int:
      value = _prefs.getInt(key);
      break;
    case double:
      value = _prefs.getDouble(key);
      break;
    case bool:
      value = _prefs.getBool(key);
      break;
    default:
      print("The value's type of $type is not supported by Shared Preferences");
  }

  return value;
}

Future<bool> checkExistenceOf(String key) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  return _prefs.containsKey(key);
}

Future<void> delete(String key) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  if (await checkExistenceOf(key)) {
    _prefs.remove(key);
  }
}

Future<void> resetSharedPreferences() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.clear();
}
