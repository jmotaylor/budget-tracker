import 'dart:async';

import 'package:budget_tracker/pages/add_entry_page.dart';
import 'package:budget_tracker/pages/reset_data_page.dart';
import 'package:budget_tracker/pages/edit_entry_page.dart';
import 'package:budget_tracker/pages/entries_page.dart';
import 'package:budget_tracker/pages/launch_page.dart';
import 'package:budget_tracker/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter_localizations/flutter_localizations.dart';

import 'localizations/localizations_text.dart';
import 'pages/setup_page.dart';
import 'pages/language_page.dart';
import 'pages/main_page.dart';

class DemoLocalizationsDelegate
    extends LocalizationsDelegate<DemoLocalizations> {
  const DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'jp'].contains(locale.languageCode);

  @override
  Future<DemoLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<DemoLocalizations>(DemoLocalizations(locale));
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;
}

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

void main() {
  runApp(
    MaterialApp(
      localizationsDelegates: [
        const DemoLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('jp'),
      ],
      title: 'Budget Tracker',
      initialRoute: '/launch',
      routes: {
        '/': (context) => MainPage(),
        '/setup-budget': (context) => SetupPage(),
        '/language': (context) => LanguagePage(),
        '/launch': (context) => LaunchPage(),
        '/entries': (context) => EntriesPage(),
        '/edit-entry': (context) => EditEntryPage(),
        AddEntryPage.routeName: (context) => AddEntryPage(),
        ResetDataPage.routeName: (context) => ResetDataPage(),
      },
      navigatorObservers: [routeObserver],
      theme: customTheme(),
    ),
  );
}
