import 'package:budget_tracker/components/custom_snackbar.dart';
import 'package:budget_tracker/helpers/conversions.dart';

class Validations {
  static double amount(value) {
    double amount = double.tryParse(removeCommas(value));

    return amount != 0 && amount != null ? amount : null;
  }

  static String notes(value) {
    return value.trim();
  }

  static Map validate({
    String amount,
    String notes,
    Function snackbarCallback,
  }) {
    Map results = {
      'validationError': false,
      'amount': null,
      'notes': null,
    };

    bool validationError = false;

    double amountValue = Validations.amount(amount);

    if (amountValue == null) {
      snackbarCallback(CustomSnackbar.show('amount'));
      validationError = true;
      results['validationError'] = validationError;

      return results;
    } else {
      results['amount'] = amountValue;
    }

    String notesValue = Validations.notes(notes ?? '');

    if (notesValue.length > 30) {
      snackbarCallback(CustomSnackbar.show('notes'));
      validationError = true;
      results['validationError'] = validationError;

      return results;
    } else {
      results['notes'] = notesValue;
    }

    return results;
  }
}
