import 'package:budget_tracker/helpers/math.dart';

dynamic convertToNumber(String value) {
  return roundDouble(double.parse(value), 2);
}

String handleDecimals(bool hasDecimals, String value) {
  if (hasDecimals) {
    return !value.contains('.') ? value + '.00' : value;
  } else {
    return value;
  }
}

String convertToProperAmountFormat(bool isDecimal, String amount) {
  double amountAsNumber = convertToNumber(amount);
  return convertToString(isDecimal, amountAsNumber);
}

String convertDoubleToProperAmountFormat(bool isDecimal, double amount) {
  double amountAsNumber = roundDouble(amount, 2);
  return convertToString(isDecimal, amountAsNumber);
}

String convertToString(bool hasDecimals, value) {
  // return addCommas(
  return hasDecimals ? value.toStringAsFixed(2) : value.toStringAsFixed(0);
  // );
}

// https://stackoverflow.com/questions/31931257/dart-how-to-add-commas-to-a-string-number
String addCommas(String amount) {
  return amount.replaceAllMapped(
      new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => "${m[1]},");
}

String removeCommas(String amount) {
  return amount.replaceAll(RegExp(r','), '');
}
