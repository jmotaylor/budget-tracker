import 'package:budget_tracker/components/app_bar_back_button.dart';
import 'package:flutter/material.dart';

import '../styles.dart';

class SubAppBar extends StatelessWidget {
  final String title;

  SubAppBar(this.title);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        // DemoLocalizations.of(context).title,
        style: Styles.appBarStyles,
      ),
      elevation: 0.0,
      leading: AppBarBackButton(),
    );
  }
}
