import 'package:budget_tracker/components/delete_dialog.dart';
import 'package:budget_tracker/controllers/entries_page_controller.dart';
import 'package:budget_tracker/models/accounting_entry.dart';
import 'package:budget_tracker/models/currency.dart';
import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';

class EntryListItem extends StatefulWidget {
  final AccountingEntry accountingEntry;
  final Currency currency;
  final refreshCallback;

  EntryListItem({this.accountingEntry, this.currency, this.refreshCallback});

  @override
  _EntryListItemState createState() => _EntryListItemState();
}

class _EntryListItemState extends State<EntryListItem> {
  EntriesPageController _controller;

  @override
  Widget build(BuildContext context) {
    _controller = EntriesPageController(
      currency: widget.currency,
      accountingEntry: widget.accountingEntry,
    );

    Map<String, String> values = _controller.formatValues();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  '/edit-entry',
                  arguments: {'controller': _controller},
                ).then((value) => widget.refreshCallback());
              },
              contentPadding: EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 8.0),
              leading: Icon(
                values['operator'] == '+' ? Icons.add : Icons.remove,
                color: Colors.white,
              ),
              title: Text(
                '${values['currencySymbol']} ${values['amount']}',
                style: Styles.largeText,
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${values['date']}',
                    style: Styles.smallText,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: values['notes'] != ''
                        ? Text(
                            '${values['notes']}',
                            style: Styles.regularText,
                          )
                        : Text(
                            'No notes entered',
                            style: Styles.verySmallTextItalicized,
                          ),
                  ),
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    color: Colors.white,
                  ),
                  child: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Styles.primaryColor,
                      ),
                      onPressed: () {
                        showDeleteDialog(
                            context: context,
                            text: 'Are you sure you want to delete this entry?',
                            confirmCallback: () {
                              Navigator.of(context).pop();
                              _controller.deleteEntry(values['key']);
                            }).then((value) => widget.refreshCallback());
                      }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
