import 'package:flutter/material.dart';

import '../styles.dart';

class DrawerItem extends StatelessWidget {
  final String title;
  final String route;

  DrawerItem({this.title, this.route});  

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(
          color: Styles.textColorDefault,
        ),
      ),
      onTap: () {
        Navigator.pop(context);
        Navigator.pushNamed(
          context,
          route
        );
      },
    );
  }
}

