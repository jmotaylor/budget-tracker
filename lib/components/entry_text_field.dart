import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class EntryTextField extends StatefulWidget {
  final String type;
  final String label;
  final dynamic value;
  final int maxLines;
  final datePicker;
  TextEditingController textEditingController;
  DateTime dateTime;

  EntryTextField({
    this.type,
    this.label,
    this.value,
    this.maxLines = 1,
    this.datePicker,
    this.textEditingController,
    this.dateTime,
  });

  @override
  _EntryTextFieldState createState() => _EntryTextFieldState();
}

class _EntryTextFieldState extends State<EntryTextField> {
  var fieldController = TextEditingController();

  DateTime dateNow;
  DateTime selectedDate;

  @override
  void initState() {
    super.initState();

    widget.textEditingController = fieldController;

    if (widget.type == 'Date') {
      widget.dateTime = dateNow = DateTime.now();
      selectedDate = null;
      // fieldController.text = DateFormat('yyyy-MM-dd – EEE').format(dateNow);
      fieldController.text = widget.value;
    }

    // fieldController.text = widget.value != null ? widget.value : '';
    fieldController.text = widget.value != null ? widget.value : '';
  }

  @override
  Widget build(BuildContext context) {
    print(widget.value);
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Styles.secondaryColor,
                blurRadius: 4.0,
                spreadRadius: 2.0,
                offset: Offset(
                  0,
                  2.0,
                ),
              ),
            ],
            color: Colors.white,
          ),
          child: widget.type == 'Date'
              ? dateField(widget.value)
              : widget.type == 'Text'
                  ? textField(widget.value)
                  : numField(widget.value)),
    );
  }

  dateField(value) {
    return TextFormField(
      controller: fieldController,
      keyboardType: TextInputType.numberWithOptions(
        decimal: false,
      ),
      style: TextStyle(fontSize: 24.0),
      decoration: InputDecoration(
        border: InputBorder.none,
        isDense: true,
        labelText: widget.label != null ? widget.label : '',
        labelStyle: TextStyle(
          color: Styles.textColorPrimary,
          fontSize: 16.0,
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      maxLines: widget.maxLines,
      onTap: () => _selectDate(context),
      readOnly: true,
    );
  }

  textField(value) {
    return TextFormField(
      controller: widget.textEditingController = fieldController,
      // keyboardType: TextInputType.numberWithOptions(
      //   decimal: false,
      // ),
      style: TextStyle(fontSize: 24.0),
      decoration: InputDecoration(
        border: InputBorder.none,
        isDense: true,
        labelText: widget.label != null ? widget.label : '',
        labelStyle: TextStyle(
          color: Styles.textColorPrimary,
          fontSize: 16.0,
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      maxLines: widget.maxLines,
      inputFormatters: [
        LengthLimitingTextInputFormatter(40),
      ],
      textInputAction: TextInputAction.done,
    );
  }

  numField(value) {
    return TextFormField(
      controller: fieldController,
      keyboardType: TextInputType.numberWithOptions(
        decimal: false,
      ),
      style: TextStyle(fontSize: 24.0),
      decoration: InputDecoration(
        border: InputBorder.none,
        isDense: true,
        labelText: widget.label != null ? widget.label : '',
        labelStyle: TextStyle(
          color: Styles.textColorPrimary,
          fontSize: 16.0,
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      maxLines: widget.maxLines,
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate == null ? dateNow : selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != dateNow) {
      setState(() {
        fieldController.text = DateFormat('yyyy-MM-dd – EEE').format(picked);
        widget.dateTime = selectedDate = picked;
      });
    }
  }
}
