import 'package:flutter/material.dart';

import '../styles.dart';

class FutureDaySavings extends StatefulWidget {
  final String day;
  final String amount;

  FutureDaySavings(this.day, this.amount);

  @override
  _FutureDaySavingsState createState() => _FutureDaySavingsState();
}

class _FutureDaySavingsState extends State<FutureDaySavings> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          widget.day,
          style: Styles.smallText,
        ),
        Text(
          widget.amount,
          style: Styles.smallText,
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }
}
