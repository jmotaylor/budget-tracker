import 'package:flutter/material.dart';

class FormButton extends StatelessWidget {
  final BuildContext context;
  final Color buttonColor;
  final Color textColor;
  final Color borderColor;
  final String text;
  final Function callback;

  FormButton({
    this.context,
    this.buttonColor,
    this.textColor,
    this.borderColor,
    this.text,
    this.callback,
  });

  @override
  Widget build(BuildContext context) {
    return new ElevatedButton(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: new Text(
          text,
          style: TextStyle(
            color: textColor,
            fontSize: 20.0,
          ),
        ),
      ),
      style: ElevatedButton.styleFrom(
        primary: buttonColor,
        side: BorderSide(
          color: borderColor,
        ),
      ),
      onPressed: () => callback(),
    );
  }
}
