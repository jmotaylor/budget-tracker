import 'package:flutter/material.dart';
import '../localizations/localizations_text.dart';

import '../styles.dart';
import 'drawer_item.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Styles.primaryColor,
        child: (ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 100.0,
              child: DrawerHeader(
                child: Text(
                  DemoLocalizations.of(context).settings,
                  style: TextStyle(
                      fontSize: Styles.standardTextSize,
                      color: Styles.textColorDefault),
                ),
                decoration: BoxDecoration(
                  color: Styles.primaryColor,
                  border: Border.all(
                    width: 0.0,
                    color: Styles.primaryColor,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Styles.primaryColor,
                    ),
                  ],
                ),
              ),
            ),
            DrawerItem(
              title: DemoLocalizations.of(context).editEntries,
              route: '/entries',
            ),
            // DrawerItem(
            // title: DemoLocalizations.of(context).editCategories,
            // route: SetupRoute(),
            // ),
            DrawerItem(
              title: DemoLocalizations.of(context).editBudget,
              route: '/setup-budget',
            ),
            DrawerItem(
              title: DemoLocalizations.of(context).language,
              route: '/language',
            ),
            DrawerItem(
              title: DemoLocalizations.of(context).resetData,
              route: '/delete-data',
            ),
            // DrawerItem(
            // title: DemoLocalizations.of(context).removeAds,
            // route: SetupRoute(),
            // ),
          ],
        )),
      ),
    );
  }
}
