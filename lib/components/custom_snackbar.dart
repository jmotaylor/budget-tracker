import 'package:flutter/material.dart';

class CustomSnackbar {
  static Widget show(text) {
    String message = '';
    String amount = 'You must enter a valid amount';
    String notes = 'Notes must be less that 30 characters';

    switch (text) {
      case 'amount':
        message = amount;
        break;
      case 'notes':
        message = notes;
        break;
      default:
        message = '';
        break;
    }
    return SnackBar(
      content: Text(message),
    );
  }
}
