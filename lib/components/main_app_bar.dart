import 'package:flutter/material.dart';

import '../styles.dart';
import '../localizations/localizations_text.dart';

class MainAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        DemoLocalizations.of(context).title,
        style: Styles.appBarStyles,
      ),
      elevation: 0.0,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            color: Styles.textColorDefault,
            icon: const Icon(Icons.menu),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: DemoLocalizations.of(context).settings,
          );
        },
      ),
    );
  }
}
