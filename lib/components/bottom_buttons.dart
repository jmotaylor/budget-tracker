import 'package:flutter/material.dart';
import '../styles.dart';

class BottomButtons extends StatelessWidget {
  final BuildContext context;
  final Widget icon;
  final String tooltip;
  final Function callback;

  BottomButtons({this.context, this.icon, this.tooltip, this.callback});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon,
      iconSize: 56.0,
      color: Styles.textColorDefault,
      tooltip: tooltip,
      // onPressed: () => callback(context),
      onPressed: () => callback(),
    );
  }
}
