import 'dart:async';

import 'package:budget_tracker/add_entry_page_arguments.dart';
import 'package:budget_tracker/controllers/main_page_controller.dart';
import 'package:budget_tracker/helpers/enums.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'future_day_savings.dart';
import 'bottom_buttons.dart';
import '../styles.dart';
import '../localizations/localizations_text.dart';

class MainDisplay extends StatefulWidget {
  final MainPageController controller;
  final Stream stream;

  MainDisplay(this.controller, this.stream);

  @override
  _MainDisplayState createState() => _MainDisplayState();
}

class _MainDisplayState extends State<MainDisplay> {
  String _balance;
  String _currencySymbol;
  String _dailyBudget;
  bool _isDecimal;
  List<String> _futureBalances;

  @override
  void initState() {
    super.initState();

    checkExistenceOf("budget").then((result) {
      if (result) {
        widget.controller.getAllValues();
      }
    });

    widget.controller.updateFigures();

    widget.stream.listen((stringValues) {
      _updateValues(stringValues);
    });
  }

  void _updateValues(Map stringValues) {
    if (stringValues["balance"] != null) {
      setState(() {
        _balance = stringValues["balance"];
      });
    }

    if (stringValues["dailyBudget"] != null) {
      setState(() {
        _dailyBudget = stringValues["dailyBudget"];
      });
    }

    if (stringValues["isDecimal"] != null) {
      setState(() {
        _isDecimal = stringValues["isDecimal"];
      });
    }

    if (stringValues["futureBalances"] != null) {
      setState(() {
        _futureBalances = stringValues["futureBalances"];
      });
    }

    if (stringValues["currencySymbol"] != null) {
      setState(() {
        _currencySymbol = stringValues["currencySymbol"];
      });
    }
  }

  _addEntry(InputType inputType, bool isDecimal) {
    Navigator.pushNamed(
      context,
      '/add-entry',
      arguments: AddEntryPageArguments(
        inputType,
        isDecimal,
      ),
    );
  }

  viewKeys() async {
    // var value = await get('accumulatedDailyBudget', String);
    var value = await get('InputType.expense5', String);
    // var value = await getKeys();
    if (value != null) {
      print(value);
    }
    // var currency = await Currency.getSavedCurrency();
    // print("Currency: ${currency['symbol']}");
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Styles.primaryColor,
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    DemoLocalizations.of(context).currentBalance,
                    style: TextStyle(
                      color: Styles.textColorDefault,
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      style: Styles.veryLargeText,
                      children: [
                        TextSpan(
                            text: _currencySymbol != null
                                ? _currencySymbol + " "
                                : null),
                        TextSpan(
                          text:
                              _balance.runtimeType != String ? "---" : _balance,
                        )
                      ],
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: DemoLocalizations.of(context).dailyBudget,
                          style: TextStyle(
                            color: Styles.textColorDefault,
                          ),
                        ),
                        TextSpan(text: " "),
                        TextSpan(
                          text: _dailyBudget ?? "---",
                          style: TextStyle(
                            color: Styles.textColorDefault,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 96.0, top: 24.0, right: 96.0, bottom: 0.0),
                child: Column(
                  children: <Widget>[
                    FutureDaySavings(
                        DateFormat('EEEE').format(
                                DateTime.now().add(new Duration(days: 1))) +
                            ":",
                        _futureBalances != null ? _futureBalances[0] : "---"),
                    FutureDaySavings(
                        DateFormat('EEEE').format(
                                DateTime.now().add(new Duration(days: 2))) +
                            ":",
                        _futureBalances != null ? _futureBalances[1] : "---"),
                    FutureDaySavings(
                        DateFormat('EEEE').format(
                                DateTime.now().add(new Duration(days: 3))) +
                            ":",
                        _futureBalances != null ? _futureBalances[2] : "---"),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 48.0, right: 48.0),
                child: Row(
                  children: <Widget>[
                    BottomButtons(
                      context: context,
                      icon: Icon(Icons.remove),
                      tooltip: DemoLocalizations.of(context).minusTooltip,
                      callback: () => _addEntry(
                        InputType.expense,
                        _isDecimal,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.atm),
                      onPressed: () => viewKeys(),
                    ),
                    BottomButtons(
                      context: context,
                      icon: Icon(Icons.add),
                      tooltip: DemoLocalizations.of(context).addTooltip,
                      callback: () => _addEntry(
                        InputType.income,
                        _isDecimal,
                      ),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ),
    );
  }
}
