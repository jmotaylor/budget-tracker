import 'package:budget_tracker/localizations/localizations_text.dart';
import 'package:flutter/material.dart';

import '../styles.dart';

class AppBarBackButton extends StatelessWidget {
  const AppBarBackButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        return IconButton(
          color: Styles.textColorDefault,
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            // Scaffold.of(context).openDrawer();
            Navigator.pop(context);
          },
          tooltip: DemoLocalizations.of(context).goBack,
        );
      },
    );
  }
}
