import 'package:flutter/material.dart';

class Styles {
  static final Color primaryColor = _hexToColor('2ec79f');
  static final Color secondaryColor = _hexToColor('16a882');
  static final Color dangerColor = _hexToColor('bc544b');
  // static final Color primaryColor = _hexToColor('47e4bb');
  static final Color textColorDefault = _hexToColor('ffffff');
  static final Color textColorPrimary = _hexToColor('555555');
  static const standardTextSize = 20.0;
  static const _veryLargeTextSize = 40.0;
  static const largeTextSize = 30.0;
  static const _smallTextSize = 16.0;
  static const verySmallTextSize = 14.0;
  // static final Color _textColorStrong = _hexToColor('000000');
  static final String fontNameDefault = 'Muli';

  static Color _hexToColor(String code) {
    return Color(int.parse(code.substring(0, 6), radix: 16) + 0xFF000000);
  }

  static final veryLargeText = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: _veryLargeTextSize,
    color: textColorDefault,
  );

  static final largeText = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: largeTextSize,
    color: textColorDefault,
  );

  static final regularText = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: standardTextSize,
    color: textColorDefault,
  );

  static final smallText = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: _smallTextSize,
    color: textColorDefault,
  );

  static final verySmallText = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: verySmallTextSize,
    color: textColorDefault,
  );

  static final verySmallTextItalicized = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: verySmallTextSize,
    fontStyle: FontStyle.italic,
    color: textColorDefault,
  );

  static final appBarStyles = TextStyle(
      fontFamily: fontNameDefault, color: textColorDefault, fontSize: 24.0);
}

// https://colorhunt.co/palette/156289
// https://colorhunt.co/palette/157018
// https://colorhunt.co/palette/158293
