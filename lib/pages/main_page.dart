import 'dart:async';

import 'package:budget_tracker/components/main_app_bar.dart';
import 'package:budget_tracker/components/main_display.dart';
import 'package:budget_tracker/components/main_drawer.dart';
import 'package:budget_tracker/controllers/main_page_controller.dart';
import 'package:budget_tracker/main.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with WidgetsBindingObserver, RouteAware {
  StreamController<Map> streamController = StreamController<Map>();
  MainPageController _controller;
  AppLifecycleState _notification;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _notification = state;
    });
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    streamController.close();
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  void didPopNext() {
    // _controller.recalculateBalances();
    _controller.getAllValues();
  }

  @override
  Widget build(BuildContext context) {
    print('last notification: $_notification');

    Locale myLocale = Localizations.localeOf(context);
    print("Device locale set to $myLocale");

    _controller = MainPageController(streamController);

    if (_notification == AppLifecycleState.resumed) {
      _controller.updateFigures();
    }

    return Scaffold(
      drawer: MainDrawer(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: MainAppBar(),
      ),
      body: MainDisplay(_controller, streamController.stream),
    );
  }
}
