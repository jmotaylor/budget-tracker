import 'package:budget_tracker/controllers/launch_controller.dart';
import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';

class LaunchPage extends StatefulWidget {
  @override
  _LaunchPageState createState() => _LaunchPageState();
}

class _LaunchPageState extends State<LaunchPage> {
  LaunchController _controller = new LaunchController();
  bool _doesBudgetExist;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => _controller.checkIfBudgetExists(context).then((result) {
        Future.delayed(const Duration(seconds: 3), () {
          setState(() {
            _doesBudgetExist = result;
          });
        });
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Text(
            "Budget Tracker",
            style: TextStyle(
              color: _doesBudgetExist == null
                  ? Styles.primaryColor
                  : _doesBudgetExist ? Colors.black : Colors.red,
              fontFamily: Styles.fontNameDefault,
              fontSize: 36.0,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );
  }
}
