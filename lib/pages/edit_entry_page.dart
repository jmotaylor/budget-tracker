import 'package:budget_tracker/components/buttons/form_button.dart';
import 'package:budget_tracker/controllers/entries_page_controller.dart';
import 'package:budget_tracker/components/entry_text_field.dart';
import 'package:budget_tracker/components/main_drawer.dart';
import 'package:budget_tracker/components/sub_app_bar.dart';
import 'package:budget_tracker/localizations/localizations_text.dart';
import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';

class EditEntryPage extends StatefulWidget {
  @override
  _EditEntryPageState createState() => _EditEntryPageState();
}

class _EditEntryPageState extends State<EditEntryPage> {
  EntriesPageController _controller;

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    _controller = arguments['controller'];

    Map<String, String> values = _controller.formatValues();

    EntryTextField amount;
    EntryTextField notes;
    EntryTextField date;

    return Scaffold(
      drawer: MainDrawer(),
      appBar: PreferredSize(
        child: SubAppBar(
            // inputType == InputType.expense ? 'Add expense' : 'Add income',
            'Edit entry'),
        preferredSize: Size.fromHeight(60.0),
      ),
      backgroundColor: Styles.primaryColor,
      body: Center(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    amount = new EntryTextField(
                      type: 'Num',
                      label: DemoLocalizations.of(context).amount,
                      value: values['amount'] ?? null,
                    ),
                    notes = new EntryTextField(
                      type: 'Text',
                      label: 'Notes',
                      maxLines: 2,
                      value: values['notes'] ?? null,
                    ),
                    date = new EntryTextField(
                      type: 'Date',
                      label: 'Date',
                      value: values['date'] ?? null,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 12.0,
                        left: 14.0,
                        right: 14.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FormButton(
                            context: context,
                            buttonColor: Colors.white,
                            textColor: Styles.secondaryColor,
                            borderColor: Styles.secondaryColor,
                            text: DemoLocalizations.of(context).cancel,
                            callback: () => Navigator.of(context).pop(),
                          ),
                          FormButton(
                            context: context,
                            buttonColor: Styles.secondaryColor,
                            textColor: Styles.textColorDefault,
                            borderColor: Styles.secondaryColor,
                            text: DemoLocalizations.of(context).save,
                            callback: () {
                              _controller.handleUpdateEntry(
                                key: values['key'],
                                entry: {
                                  'amount': amount,
                                  'notes': notes,
                                  'date': date,
                                },
                                snackbarCallback: (snackbar) =>
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackbar),
                                popCallback: () => Navigator.of(context).pop(),
                              );
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
