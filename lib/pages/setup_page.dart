import 'package:budget_tracker/components/buttons/form_button.dart';
import 'package:budget_tracker/components/sub_app_bar.dart';
import 'package:budget_tracker/controllers/setup_page_controller.dart';
import 'package:budget_tracker/models/currency.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../styles.dart';
import '../localizations/localizations_text.dart';

class SetupPage extends StatefulWidget {
  @override
  _SetupPageState createState() => _SetupPageState();
}

class _SetupPageState extends State<SetupPage> {
  String dropdownValue;
  List<Currency> _currencies = Currency.getCurrencies();
  List<DropdownMenuItem<Currency>> _dropdownMenuItems;
  Currency _selectedCurrency;
  bool _appBarToggle;

  SetupPageController _controller = new SetupPageController();
  final budgetController = TextEditingController();
  Currency _currency;

  @override
  void initState() {
    super.initState();
    _dropdownMenuItems = buildDropdownMenuItems(_currencies);
    _selectedCurrency = _dropdownMenuItems[0].value;
    _appBarToggle = true;
    _setupBudget();
  }

  Future<void> setCurrency(Currency currency) async {
    setState(() {
      _selectedCurrency = _dropdownMenuItems[currency.id].value;
    });
  }

  Future<void> _setupBudget() async {
    budgetController.text = await _controller.getBudgetIfExists(setCurrency);
    setState(() {
      _appBarToggle = _controller.initialSetup;
    });
  }

  List<DropdownMenuItem<Currency>> buildDropdownMenuItems(List currencies) {
    List<DropdownMenuItem<Currency>> items = List();
    for (Currency currency in currencies) {
      items.add(
        DropdownMenuItem(
          value: currency,
          child: Padding(
            padding: const EdgeInsets.only(left: 6.0),
            child: Text(currency.name),
          ),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Currency selectedCurrency) {
    setState(() {
      _selectedCurrency = selectedCurrency;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async =>
          SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
      child: Scaffold(
        appBar: _appBarToggle
            ? _appBarWithoutBackButton()
            : _appBarWithBackButton(),
        body: Container(
          color: Styles.primaryColor,
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _question(),
              _monthlyBudgetTextfield(),
              _currencyDropdown(),
              _continueButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _question() => Text(
        DemoLocalizations.of(context).budgetSetupText,
        style: TextStyle(
          color: Styles.textColorDefault,
          fontSize: 18.0,
        ),
      );

  Widget _monthlyBudgetTextfield() => Container(
        width: 200.0,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Styles.secondaryColor,
              blurRadius: 4.0,
              spreadRadius: 2.0,
              offset: Offset(
                0,
                2.0,
              ),
            ),
          ],
          color: Colors.white,
        ),
        child: TextFormField(
          controller: budgetController,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.numberWithOptions(
            decimal: false,
          ),
          style: TextStyle(fontSize: 30.0),
          decoration: InputDecoration(
            border: InputBorder.none,
            isDense: true,
            labelText: DemoLocalizations.of(context).amount,
            labelStyle: TextStyle(
              color: Styles.textColorPrimary,
              fontSize: 16.0,
            ),
            filled: true,
            fillColor: Colors.white,
          ),
        ),
      );

  Widget _currencyDropdown() => Container(
        height: 64.0,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Styles.secondaryColor,
              blurRadius: 4.0,
              spreadRadius: 2.0,
              offset: Offset(
                0,
                2.0,
              ),
            ),
          ],
          color: Colors.white,
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            value: _selectedCurrency,
            items: _dropdownMenuItems,
            onChanged: onChangeDropdownItem,
          ),
        ),
      );

  Widget _continueButton() => ButtonTheme(
        height: 50.0,
        child: FormButton(
            context: context,
            buttonColor: Styles.secondaryColor,
            textColor: Styles.textColorDefault,
            borderColor: Styles.secondaryColor,
            text: DemoLocalizations.of(context).continueText,
            callback: () {
              _controller.handleCreateBudget(
                  amount: budgetController.text,
                  currency: _selectedCurrency,
                  snackbarCallback: (snackbar) =>
                      ScaffoldMessenger.of(context).showSnackBar(snackbar),
                  navigatorCallback: () =>
                      Navigator.pushReplacementNamed(context, '/'));
            }),
      );

  Widget _appBarWithBackButton() => PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: SubAppBar(DemoLocalizations.of(context).budgetSetupTitle),
      );

  Widget _appBarWithoutBackButton() => AppBar(
        centerTitle: true,
        leading: Container(),
        title: Text(
          DemoLocalizations.of(context).budgetSetupTitle,
          style: Styles.appBarStyles,
        ),
        elevation: 0.0,
      );
}
