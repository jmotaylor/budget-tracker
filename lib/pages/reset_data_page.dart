import 'package:budget_tracker/components/delete_dialog.dart';
import 'package:budget_tracker/components/sub_app_bar.dart';
import 'package:budget_tracker/localizations/localizations_text.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';
import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';

class ResetDataPage extends StatefulWidget {
  static const routeName = '/delete-data';

  @override
  _ResetDataPageState createState() => _ResetDataPageState();
}

class _ResetDataPageState extends State<ResetDataPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: SubAppBar(
          DemoLocalizations.of(context).resetData,
        ),
        preferredSize: Size.fromHeight(60.0),
      ),
      body: Container(
        color: Styles.primaryColor,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(right: 24.0, bottom: 24.0, left: 24.0),
              child: Text(
                'To reset your data, press and hold the button below',
                style: Styles.regularText,
              ),
            ),
            ElevatedButton(
              onPressed: () {},
              onLongPress: () {
                showDeleteDialog(
                    context: context,
                    text: 'Are you sure you want to reset your data?',
                    confirmCallback: () {
                      Navigator.of(context).pop();
                      resetSharedPreferences();
                    }).then((value) => moveToSetupBudget(context));
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "RESET DATA",
                  style: Styles.regularText,
                ),
              ),
              style: ElevatedButton.styleFrom(
                primary: Styles.dangerColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void moveToSetupBudget(context) {
    Navigator.pushNamedAndRemoveUntil(
      context,
      '/setup-budget',
      (Route<dynamic> route) => false,
    );
  }
}
