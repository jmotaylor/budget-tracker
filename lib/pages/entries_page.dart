import 'dart:async';

import 'package:budget_tracker/components/entry_list_item.dart';
import 'package:budget_tracker/components/main_drawer.dart';
import 'package:budget_tracker/components/sub_app_bar.dart';
import 'package:budget_tracker/controllers/entries_page_controller.dart';
import 'package:budget_tracker/models/accounting_entry.dart';
import 'package:budget_tracker/models/currency.dart';
import 'package:budget_tracker/styles.dart';

import 'package:flutter/material.dart';

class EntriesPage extends StatefulWidget {
  @override
  _EntriesPageState createState() => _EntriesPageState();
}

class _EntriesPageState extends State<EntriesPage> {
  StreamController<List> streamController = StreamController<List>();
  EntriesPageController _controller;
  Currency _currency;

  @override
  void initState() {
    super.initState();

    getCurrency();
  }

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  Future<void> getCurrency() async {
    _currency = await Currency.getSavedCurrency();
  }

  void refreshEntriesPage() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _controller = EntriesPageController(streamController: streamController);

    _controller.getEntries();

    Widget buildList(List<AccountingEntry> data) {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) {
            return EntryListItem(
                accountingEntry: data[index],
                currency: _currency,
                refreshCallback: refreshEntriesPage);
          });
    }

    return Scaffold(
      drawer: MainDrawer(),
      appBar: PreferredSize(
        child: SubAppBar('Edit Entries'),
        preferredSize: Size.fromHeight(80.0),
      ),
      backgroundColor: Styles.primaryColor,
      body: Container(
        color: Styles.primaryColor,
        child: StreamBuilder(
          stream: streamController.stream,
          builder: (BuildContext context, snapshot) {
            if (snapshot.hasData) {
              return buildList(snapshot.data);
            } else {
              return new Text("No data");
            }
          },
        ),
      ),
    );
  }
}
