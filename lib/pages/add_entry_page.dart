import 'package:budget_tracker/components/buttons/form_button.dart';
import 'package:budget_tracker/components/entry_text_field.dart';
import 'package:budget_tracker/components/main_drawer.dart';
import 'package:budget_tracker/components/sub_app_bar.dart';
import 'package:budget_tracker/controllers/add_entry_page_controller.dart';
import 'package:budget_tracker/add_entry_page_arguments.dart';
import 'package:budget_tracker/helpers/enums.dart';
import 'package:budget_tracker/localizations/localizations_text.dart';
import 'package:budget_tracker/styles.dart';
import 'package:flutter/material.dart';

class AddEntryPage extends StatefulWidget {
  static const routeName = '/add-entry';

  @override
  _AddEntryPageState createState() => _AddEntryPageState();
}

class _AddEntryPageState extends State<AddEntryPage> {
  AddEntryPageController controller = new AddEntryPageController();
  InputType inputType;
  bool isDecimal;
  DateTime dateNow = DateTime.now();

  @override
  Widget build(BuildContext context) {
    EntryTextField amount;
    EntryTextField notes;
    EntryTextField date;

    final AddEntryPageArguments args =
        ModalRoute.of(context).settings.arguments;

    inputType = args.inputType;
    isDecimal = args.isDecimal;

    return Scaffold(
      drawer: MainDrawer(),
      appBar: PreferredSize(
        child: SubAppBar(
          inputType == InputType.expense ? 'Add expense' : 'Add income',
        ),
        preferredSize: Size.fromHeight(60.0),
      ),
      backgroundColor: Styles.primaryColor,
      body: Center(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    amount = new EntryTextField(
                      type: 'Num',
                      label: DemoLocalizations.of(context).amount,
                    ),
                    notes = EntryTextField(
                      type: 'Text',
                      label: 'Notes',
                      maxLines: 2,
                    ),
                    date = EntryTextField(
                      type: 'Date',
                      label: 'Date',
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 12.0,
                        left: 14.0,
                        right: 14.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FormButton(
                            context: context,
                            buttonColor: Colors.white,
                            textColor: Styles.secondaryColor,
                            borderColor: Styles.secondaryColor,
                            text: DemoLocalizations.of(context).cancel,
                            callback: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FormButton(
                            context: context,
                            buttonColor: Styles.secondaryColor,
                            textColor: Styles.textColorDefault,
                            borderColor: Styles.secondaryColor,
                            text: DemoLocalizations.of(context).save,
                            callback: () {
                              controller.handleAddEntry(
                                  entry: {
                                    'amount': amount,
                                    'inputType': inputType,
                                    'date': date,
                                    'notes': notes,
                                    'isDecimal': isDecimal
                                  },
                                  snackbarCallback: (snackbar) =>
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackbar),
                                  popCallback: () =>
                                      Navigator.of(context).pop());
                            },
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
