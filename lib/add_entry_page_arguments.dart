import 'package:budget_tracker/helpers/enums.dart';

class AddEntryPageArguments {
  // final MainPageController controller;
  final InputType inputType;
  final bool isDecimal;

  AddEntryPageArguments(this.inputType, this.isDecimal);
}
