import 'dart:async';
import 'dart:convert';

import 'package:budget_tracker/helpers/enums.dart';
import 'package:budget_tracker/helpers/validations.dart';
import 'package:budget_tracker/models/accounting_entry.dart';
import 'package:budget_tracker/models/currency.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';
import 'package:budget_tracker/helpers/conversions.dart';

import 'package:intl/intl.dart';
import 'package:budget_tracker/helpers/string_extension.dart';

class EntriesPageController {
  StreamController streamController;
  Currency currency;
  AccountingEntry accountingEntry;

  EntriesPageController(
      {this.streamController, this.currency, this.accountingEntry});

  void getEntries() async {
    List entriesKeys = await getEntriesKeys();
    List<AccountingEntry> accountingEntries = [];

    for (String entryKey in entriesKeys) {
      String entryValue = await get(entryKey, String);
      dynamic json = jsonDecode(entryValue);
      AccountingEntry entry = AccountingEntry.fromJson(json);
      accountingEntries.add(entry);
    }

    streamController.add(accountingEntries);
  }

  Future<List> getEntriesKeys() async {
    List<String> incomeKeys = await getAllKeysWithPrefixOf(InputType.income);
    List<String> expenseKeys = await getAllKeysWithPrefixOf(InputType.expense);
    List entries = [...incomeKeys, ...expenseKeys];

    return entries;
  }

  Map<String, String> formatValues() {
    Map<String, String> values = {};

    String _key = accountingEntry.key;
    String _inputType = accountingEntry.inputType
        .toString()
        .replaceAll('InputType.', '')
        .capitalize();
    String _date =
        DateFormat('yyyy-MM-dd – EEE').format(accountingEntry.dateCreated);
    String _operator =
        accountingEntry.inputType == InputType.income ? '+' : '-';
    String _currencySymbol = currency.symbol;
    String _amount = accountingEntry.amount;
    String _notes = accountingEntry.notes;

    values['key'] = _key;
    values['inputType'] = _inputType;
    values['date'] = _date;
    values['operator'] = _operator;
    values['currencySymbol'] = _currencySymbol;
    values['amount'] = addCommas(_amount);
    values['notes'] = _notes;

    return values;
  }

  void handleUpdateEntry({
    String key,
    Map<String, dynamic> entry,
    Function snackbarCallback,
    Function popCallback,
  }) {
    Map results = Validations.validate(
      amount: entry['amount'].textEditingController.text,
      notes: entry['notes'].textEditingController.text,
      snackbarCallback: snackbarCallback,
    );

    if (!results['validationError']) {
      results['date'] = entry['date'].dateTime;

      updateEntry(
        key: key,
        values: results,
      );

      popCallback();
    }
  }

  void updateEntry({String key, Map values}) async {
    bool keyExists = await checkExistenceOf(key);

    if (keyExists) {
      String value = await get(key, String);
      dynamic json = jsonDecode(value);
      AccountingEntry entry = AccountingEntry.fromJson(json);

      String stringAmount = convertDoubleToProperAmountFormat(
          currency.decimals, values['amount']);
      // String amount = convertToProperAmountFormat(
      //     currency.decimals, removeCommas(values['amount']));

      entry.amount = stringAmount;
      entry.notes = values['notes'];
      entry.dateCreated = values['date'];

      entry.save(key);
    }
  }

  void deleteEntry(String key) async {
    delete(key);
  }
}
