import 'package:budget_tracker/helpers/conversions.dart';
import 'package:budget_tracker/helpers/validations.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';
import 'package:budget_tracker/helpers/enums.dart';
import 'package:budget_tracker/models/accounting_entry.dart';

class AddEntryPageController {
  AddEntryPageController();

  void addEntry({
    InputType inputType,
    DateTime dateValue,
    double amountValue,
    String notesValue,
    bool isDecimal,
  }) {
    if (notesValue == null) {
      notesValue = '';
    }

    saveEntry(
      inputType: inputType,
      dateTime: dateValue,
      amount: amountValue,
      notes: notesValue,
      isDecimal: isDecimal,
    );
  }

  Future<void> saveEntry({
    InputType inputType,
    DateTime dateTime,
    double amount,
    String notes,
    bool isDecimal,
  }) async {
    String key = await createKey(inputType);

    String stringAmount = convertDoubleToProperAmountFormat(isDecimal, amount);

    AccountingEntry entry = AccountingEntry(
        key: key,
        inputType: inputType,
        amount: stringAmount,
        notes: notes,
        dateCreated: dateTime);
    entry.save(key);
  }

  void handleAddEntry({
    Map<String, dynamic> entry,
    Function snackbarCallback,
    Function popCallback,
  }) {
    Map results = Validations.validate(
      amount: entry['amount'].textEditingController.text,
      notes: entry['notes'].textEditingController.text,
      snackbarCallback: snackbarCallback,
    );

    if (!results['validationError']) {
      addEntry(
        inputType: entry['inputType'],
        dateValue: entry['date'].dateTime,
        amountValue: results['amount'],
        notesValue: results['notes'],
        isDecimal: entry['isDecimal'],
      );

      popCallback();
    }
  }
}
