import 'package:budget_tracker/helpers/conversions.dart';
import 'package:budget_tracker/helpers/validations.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';
import 'package:budget_tracker/models/currency.dart';
import 'package:budget_tracker/models/monthly_budget.dart';

class SetupPageController {
  Function setCurrencyCallback;
  bool initialSetup = false;

  SetupPageController();

  void handleCreateBudget({
    String amount,
    Currency currency,
    Function snackbarCallback,
    Function navigatorCallback,
  }) {
    Map results = Validations.validate(
      amount: amount,
      snackbarCallback: snackbarCallback,
    );

    if (!results['validationError']) {
      create(
        amount: results['amount'],
        currency: currency,
      );
      navigatorCallback();
    }
  }

  void create({double amount, Currency currency}) async {
    String stringAmount =
        convertDoubleToProperAmountFormat(currency.decimals, amount);
    MonthlyBudget monthlyBudget = MonthlyBudget(stringAmount, currency.id);
    monthlyBudget.save();
  }

  Future<String> getBudgetIfExists(Function setCurrencyCallback) async {
    String value = await get("budget", String);

    if (value != null && value != '') {
      setCurrencyCallback(await getCurrency());
      value = addCommas(value);
    } else {
      value = '';
      this.initialSetup = true;
    }

    return value;
  }

  Future<Currency> getCurrency() async {
    Currency currency = await Currency.getSavedCurrency();

    return currency;
  }
}
