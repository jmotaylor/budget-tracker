import 'dart:async';
import 'dart:convert';

import 'package:budget_tracker/helpers/conversions.dart';
import 'package:budget_tracker/helpers/enums.dart';
import 'package:budget_tracker/models/accounting_entry.dart';
import 'package:budget_tracker/models/currency.dart';
import 'package:budget_tracker/sp_functions/sp_functions.dart';

class MainPageController {
  List<String> futureBalances;

  int _futureDays = 3;

  StreamController streamController;

  MainPageController(this.streamController);

  Future<void> getAllValues() async {
    Map stringValues = {};

    Currency currency = await Currency.getSavedCurrency();
    String currencySymbol = currency.symbol;
    bool hasDecimals = currency.decimals;

    dynamic dailyBudgetAsNumber = await setDailyBudget(hasDecimals);
    String dailyBudget =
        addCommas(convertToString(hasDecimals, dailyBudgetAsNumber));

    dynamic balanceAsNumber =
        await setBalance(dailyBudgetAsNumber, hasDecimals);
    dynamic balance = addCommas(convertToString(hasDecimals, balanceAsNumber));

    List futureBalances =
        setFutureBalances(balanceAsNumber, dailyBudgetAsNumber, hasDecimals);

    stringValues["currencySymbol"] = currencySymbol;
    stringValues["dailyBudget"] = dailyBudget;
    stringValues["balance"] = balance;
    stringValues["futureBalances"] = futureBalances;
    stringValues["isDecimal"] = currency.decimals;

    streamController.add(stringValues);
  }

  Future<dynamic> setDailyBudget(bool hasDecimals) async {
    String rawBudget = await get("budget", String) ?? 0;

    dynamic dailyBudgetAsNumber = (convertToNumber(rawBudget) * 12) / 365;

    // if (!hasDecimals && dailyBudgetAsNumber.runtimeType == double) {
    //   dailyBudgetAsNumber = dailyBudgetAsNumber.round();
    // }

    // if (hasDecimals) {
    //   dailyBudgetAsNumber = roundDouble(dailyBudgetAsNumber, 2);
    // }

    return dailyBudgetAsNumber;
  }

  Future<dynamic> setBalance(
      dynamic dailyBudgetAsNumber, bool hasDecimals) async {
    dynamic incomeExpenseTotal = await calculateAccountingEntries(hasDecimals);
    dynamic accumulatedDailyBudgetAsNumber;

    String accumulatedDailyBudget = await get("accumulatedDailyBudget", String);

    if (accumulatedDailyBudget == null) {
      accumulatedDailyBudgetAsNumber = dailyBudgetAsNumber;
      await saveMap({
        "accumulatedDailyBudget":
            convertToString(hasDecimals, accumulatedDailyBudgetAsNumber)
      });
    } else {
      accumulatedDailyBudgetAsNumber = convertToNumber(accumulatedDailyBudget);
    }

    return incomeExpenseTotal + accumulatedDailyBudgetAsNumber;
  }

  Future<dynamic> calculateAccountingEntries(bool hasDecimals) async {
    List<String> incomeKeys = await getAllKeysWithPrefixOf(InputType.income);
    List<String> expenseKeys = await getAllKeysWithPrefixOf(InputType.expense);
    List<String> entryKeys = new List.from(incomeKeys)..addAll(expenseKeys);
    List<dynamic> amounts = [];
    dynamic total;

    for (String entryKey in entryKeys) {
      String entryValue = await get(entryKey, String);
      dynamic json = jsonDecode(entryValue);
      AccountingEntry entry = AccountingEntry.fromJson(json);
      dynamic amount = entry.inputType == InputType.expense
          ? convertToNumber(entry.amount) * -1
          : convertToNumber(entry.amount);
      amounts.add(amount);
    }

    if (amounts != null && amounts.length > 0) {
      total = amounts.reduce((value, element) => value + element);
    } else {
      total = 0;
    }
    print("accounting entries total: $total");
    return total;
  }

  List<dynamic> setFutureBalances(
      dynamic balanceAsNumber, dynamic dailyBudgetAsNumber, bool hasDecimals) {
    dynamic totalBalanceAsNumber = balanceAsNumber;
    futureBalances = [];

    for (var i = 0; i < _futureDays; i++) {
      totalBalanceAsNumber += dailyBudgetAsNumber;
      String totalBalance =
          addCommas(convertToString(hasDecimals, totalBalanceAsNumber));
      futureBalances.add(totalBalance);
    }

    return futureBalances;
  }

  Future<void> saveNextMidnight() async {
    DateTime now = new DateTime.now();
    DateTime nextMidnight = new DateTime(now.year, now.month, now.day + 1);
    saveMap({"nextMidnight": nextMidnight.toString()});
  }

  Future<void> compareDates(DateTime now, DateTime nextMidnight) async {
    // DateTime nextMidnight = DateTime.parse(await get("nextMidnight", String));
    // DateTime now = DateTime.now();
    // bool isBeforeNextMidnight = now.isBefore(nextMidnight);

    // if (!isBeforeNextMidnight) {
    int daysPast = calculateDaysPast(now, nextMidnight);
    Map stringValues = await recalculateBalancesAfterNewDay(daysPast);

    await saveMap(
        {"accumulatedDailyBudget": stringValues["accumulatedDailyBudget"]});
    await saveNextMidnight();

    // await saveMap({"test": false});
    // print("Should have saved false");

    streamController.add(stringValues);
    // }
  }

  int calculateDaysPast(DateTime now, DateTime nextMidnight) {
    print('date now: $now');
    print('saved next midnight: $nextMidnight');

    int daysPast;
    dynamic difference = now.difference(nextMidnight).inSeconds;

    print('difference in seconds: $difference');

    dynamic daysPastAsNumber = difference / 86400;

    print('days past: $daysPastAsNumber');

    if (daysPastAsNumber.runtimeType == double) {
      daysPast = (difference ~/ 86400) + 1;
      print('this is a double');
      print('days past: $daysPast');
    } else if (daysPastAsNumber.runtimeType == int) {
      daysPast = daysPastAsNumber;
      print('this is an int');
      print('days past: $daysPast');
    }

    return daysPast;
  }

  Future<Map> recalculateBalancesAfterNewDay(int daysPast) async {
    // print("days past (recalculateBalanceAfterNewDay): $daysPast");
    Map stringValues = {};

    Currency currency = await Currency.getSavedCurrency();
    bool hasDecimals = currency.decimals;

    dynamic dailyBudgetAsNumber = await setDailyBudget(hasDecimals);
    // print("daily budget as number: $dailyBudgetAsNumber");

    dynamic balanceAsNumber =
        await setBalance(dailyBudgetAsNumber, hasDecimals);
    // print("balance as number: $balanceAsNumber");

    // print(
    //     'daily budget as number * days past = ${dailyBudgetAsNumber * daysPast}');
    // print('+ $balanceAsNumber =');

    dynamic newBalanceAsNumber =
        (dailyBudgetAsNumber * daysPast) + balanceAsNumber;
    // print("new balance as number: $newBalanceAsNumber");

    String balance =
        addCommas(convertToString(hasDecimals, newBalanceAsNumber));
    // print("balance to save is: $balance");

    dynamic accumulatedDailyBudget =
        await get('accumulatedDailyBudget', String);
    dynamic accumulatedDailyBudgetAsNumber =
        convertToNumber(accumulatedDailyBudget);
    accumulatedDailyBudgetAsNumber += (dailyBudgetAsNumber * daysPast);
    accumulatedDailyBudget =
        convertToString(hasDecimals, accumulatedDailyBudgetAsNumber);

    List futureBalances =
        setFutureBalances(newBalanceAsNumber, dailyBudgetAsNumber, hasDecimals);
    // print("future balances: $futureBalances");

    stringValues["balance"] = balance;
    stringValues["accumulatedDailyBudget"] = accumulatedDailyBudget;
    stringValues["futureBalances"] = futureBalances;

    // print("string values: $stringValues");
    return stringValues;
  }

  // Future<void> recalculateBalances() async {
  // Map stringValues = {};

  // Currency currency = await Currency.getSavedCurrency();
  // bool hasDecimals = currency.decimals;

  // dynamic dailyBudgetAsNumber = await setDailyBudget(hasDecimals);

  // dynamic balanceAsNumber =
  //     await setBalance(dailyBudgetAsNumber, hasDecimals);
  // String balance = addCommas(convertToString(hasDecimals, balanceAsNumber));

  // List futureBalances =
  //     setFutureBalances(balanceAsNumber, dailyBudgetAsNumber, hasDecimals);

  // stringValues["balance"] = balance;
  // stringValues["futureBalances"] = futureBalances;

  // streamController.add(stringValues);
  // }

  void updateFigures() {
    checkExistenceOf("nextMidnight").then((result) {
      print("nextMidnight existence: $result");
      get("nextMidnight", String).then((value) {
        if (value != null) {
          print("nextMidnight value: $value");
          DateTime nextMidnight = DateTime.parse(value);
          DateTime now = DateTime.now();
          bool isBeforeNextMidnight = now.isBefore(nextMidnight);

          if (!isBeforeNextMidnight) {
            compareDates(now, nextMidnight);
          } else {
            saveNextMidnight();
          }
        }
        saveNextMidnight();
      });
    });
  }
}
