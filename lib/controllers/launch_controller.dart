import 'package:budget_tracker/sp_functions/sp_functions.dart';
import 'package:flutter/material.dart';

class LaunchController {
  Future<bool> checkIfBudgetExists(context) async {
    bool doesBudgetExist = await checkExistenceOf("budget");

    Future.delayed(Duration(seconds: 3), () {
      doesBudgetExist
          ? _moveToNextPage(context, '/')
          : _moveToNextPage(context, '/setup-budget');
    });

    return doesBudgetExist;
  }

  void _moveToNextPage(BuildContext context, String route) async {
    Future.delayed(Duration(seconds: 1), () {
      Navigator.pushReplacementNamed(
        context,
        route,
      );
    });
  }
}
