import 'package:flutter/material.dart';

class DemoLocalizations {
  DemoLocalizations(this.locale);

  final Locale locale;

  static DemoLocalizations of(BuildContext context) {
    return Localizations.of<DemoLocalizations>(context, DemoLocalizations);
  }

  //TODO: Complete language entries
  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'title': 'Budget Tracker',
      'minusTooltip': 'Add an expense',
      'addTooltip': 'Add income',
      'settings': 'Settings',
      'edit_categories': 'Edit categories',
      'remove_ads': 'Remove ads',
      'edit_entries': 'Edit entries',
      'current_balance': 'Your current balance',
      'edit_budget': 'Edit monthly budget',
      'budget_setup_title': 'Your monthly budget',
      'budget_setup_text': 'How much can you spend every month?',
      'continueText': 'Continue',
      'amount': 'Amount',
      'daily_budget': 'Daily Budget:',
      'language': 'Language',
      'reset_data': 'Reset data',
      'go_back': "Go back",
      'cancel': "Cancel",
      'save': "Save",
    },
    'jp': {
      'title': '予算トラッカー',
      'minusTooltip': '支出を追加',
      'addTooltip': '収入を追加',
      'settings': '設定',
      'edit_categories': 'カテゴリーを編集',
      'remove_ads': '広告をリムーブする',
      'edit_entries': 'エントリーを編集 ',
      'current_balance': '現在の残高',
      'edit_budget': '予算を編集',
      'budget_setup_title': '毎月の予算',
      'budget_setup_text': '１ヶ月いくらで生活していますか？',
      'continueText': 'つづく',
      'amount': '金額',
      'daily_budget': '１日の予算:',
      'language': '言語',
      'reset_data': 'データを削除',
      'go_back': '戻る',
      'cancel': "キャンセル",
      'save': "保存",
    },
  };

  String get title {
    return _localizedValues[locale.languageCode]['title'];
  }

  String get minusTooltip {
    return _localizedValues[locale.languageCode]['minusTooltip'];
  }

  String get addTooltip {
    return _localizedValues[locale.languageCode]['addTooltip'];
  }

  String get settings {
    return _localizedValues[locale.languageCode]['settings'];
  }

  String get editCategories {
    return _localizedValues[locale.languageCode]['edit_categories'];
  }

  String get removeAds {
    return _localizedValues[locale.languageCode]['remove_ads'];
  }

  String get editEntries {
    return _localizedValues[locale.languageCode]['edit_entries'];
  }

  String get currentBalance {
    return _localizedValues[locale.languageCode]['current_balance'];
  }

  String get editBudget {
    return _localizedValues[locale.languageCode]['edit_budget'];
  }

  String get budgetSetupTitle {
    return _localizedValues[locale.languageCode]['budget_setup_title'];
  }

  String get budgetSetupText {
    return _localizedValues[locale.languageCode]['budget_setup_text'];
  }

  String get continueText {
    return _localizedValues[locale.languageCode]['continueText'];
  }

  String get amount {
    return _localizedValues[locale.languageCode]['amount'];
  }

  String get dailyBudget {
    return _localizedValues[locale.languageCode]['daily_budget'];
  }

  String get language {
    return _localizedValues[locale.languageCode]['language'];
  }

  String get resetData {
    return _localizedValues[locale.languageCode]['reset_data'];
  }

  String get goBack {
    return _localizedValues[locale.languageCode]['go_back'];
  }

  String get cancel {
    return _localizedValues[locale.languageCode]['cancel'];
  }

  String get save {
    return _localizedValues[locale.languageCode]['save'];
  }
}
